clear all
M=figure 

load divStrain3d.data;

D=divStrain3d;

s1=D(:,1);
s2=D(:,2);
sa1=D(:,3);
sa2=D(:,4);
sb1=D(:,5);
sb2=D(:,6);

Stot=union(s1,s2);
dS1tot=union(sa1-s1,sb1-s1);
dS2tot=union(sa2-s2,sb2-s2);
dStot=union(dS1tot,dS2tot);
NRMSE=sqrt(sum((dStot.^2))/numel(dStot))/mean(Stot);

NRMSE
%subplot(2,1,1);
%plot(s1,s1,'k-','MarkerSize',1);
%hold on;
plot(s2,sa2,'r+','MarkerSize',3);
hold on;
plot(s2,sb2,'r+','MarkerSize',3);
plot(s1,sa1,'k+','MarkerSize',3);
plot(s1,sb1,'k+','MarkerSize',3);

axis square;
xlim([0.03 0.04])
ylim([0.03 0.04])
xlabel('\color{black}S1,\color{red}S2');
ylabel('\color{black}Sa1, Sb1, \color{red}Sa2, Sb2');


ax=gca;
ax.XTick=[0.03 0.035 0.04];
ax.YTick=[0.03 0.035 0.04];
set(ax,'FontSize',22);


% title(['\fontsize{16}black {\color{magenta}magenta '...
% '\color[rgb]{0 .5 .5}teal \color{red}red} black again'])

% subplot(2,1,2);
% plot(s2,s2,'k-','MarkerSize',1);
% hold on;
% plot(s2,sa2,'b+','MarkerSize',1);
% hold on;
% plot(s2,sb2,'r+','MarkerSize',1);
% axis square;
% xlim([0.03 0.04])
% ylim([0.03 0.04])
% xlabel('S2');
% ylabel('Sa2, Sb2');

hold off;


% for i=1:size(s1)
% teta(i)=(teta(i)*180)/3.14;
% end
% 
% for i=1:size(s1)
% missesS(i,1)=sqrt(s1(i)*s1(i)+s2(i)*s2(i)-s1(i)*s2(i));
% end
% shift=400;
% for i=1:size(s1)
% s2(i)=s2(i)+shift;
% end
% 
% 
% 
% 
% plot(teta,s1,'r')
% hold on
% plot(teta,s2,'g')
% %plot(teta,missesS)
% 
% 
% xlim([0 180])
% ylim([520 570])
% xlabel('anisotropy direction (deg)');
% ylabel('stress (KPa)')
% 
% load fig2Dshell20.data;
% Ds=fig2Dshell20;
% 
% tetas=Ds(:,1);
% s1s=Ds(:,3);
% s2s=Ds(:,4);
% for i=1:size(s1s)
% tetas(i)=(tetas(i)*180)/3.14;
% end
% 
% for i=1:size(s1s)
% missesSs(i,1)=sqrt(s1s(i)*s1s(i)+s2s(i)*s2s(i)-s1s(i)*s2s(i));
% end
% for i=1:size(s1s)
% s2s(i)=s2s(i)+shift;
% end
% 
% 
% 
% plot(tetas,s1s,'xr')
% hold on
% plot(tetas,s2s,'xg')
% %plot(tetas,missesSs,'xb')
% legend('stress1 TRBS','stress2 TRBS','stress1 shell','stress2 shell')